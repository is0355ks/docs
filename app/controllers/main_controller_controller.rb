class MainControllerController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    if params[:lang] then
      doc = Doc.find_by(lang: params[:lang])
      jsonDoc = JSON.parse(doc.data)
      #ここで分岐処理(404エラー)
      @data = jsonDoc
    else
      doc = Doc.find_by(lang: "shell")
      jsonDoc = JSON.parse(doc.data)
      @data = jsonDoc
    end
  end

  def debug
    doc = Doc.find_by(lang: "shell")
    json_data = JSON.parse(doc.data)
    render :json => json_data[0]
  end

  def update
  end

  def updatePost
    render "updatePost"
    json_request = JSON.parse(
      request.body.read
    )
    if json_request then
      json_request.each do |json|
        doc = Doc.new
        doc.lang = json['lang']
        doc.data = json['data']
        doc.save
      end
    end
  end
end
