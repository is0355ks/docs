class CreateDocs < ActiveRecord::Migration[5.1]
  def change
    create_table :docs do |t|
      t.string :lang
      t.text :data

      t.timestamps
    end
  end
end
