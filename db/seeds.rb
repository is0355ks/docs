# coding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: 'Lord of the Rings' }])
#   Character.create(name: "Luke", movie: movies.first)

Doc.create(:lang => 'shell', :data => '[{
      "title": "test",
      "text": "hogehoge",
      "code": "namazu",
      "tag": "test",
      "group": "namako"
    },
    {
      "title": "test2",
      "text": "hogehage",
      "code": "namaneko",
      "tag": "nyan",
      "group": "namako"
    }]')

Doc.create(:lang => 'ruby', :data => '[{
      "title": "test",
      "text": "hogehoge",
      "code": "namazu",
      "tag": "text",
      "group": "namako"
    }]')
