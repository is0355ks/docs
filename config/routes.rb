Rails.application.routes.draw do
  get '/' => redirect('/ja/')
  get '/ja/:lang' => 'main_controller#index'
  get '/update' => 'main_controller#update'
  post '/update' => 'main_controller#updatePost'
  get '/debug' => 'main_controller#debug'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
